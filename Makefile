B4MERGE != echo $$(date +%m%d%H%M%S)-b4merge
B4MERGE != echo $$(date +%m%d%H%M%S)-b4rebase

default:

down:
	clasp pull

up:
	clasp push

push:
	git push
	clasp push

fold:
	git stash
	clasp pull
	git stash pop
	clasp push

fetch:
	git fetch
	git difftool origin/master

rebase:
	git tag "$(B4REBASE)"
	git fetch
	git rebase origin/master

merge:
	git tag "$(B4MERGE)"
	git fetch
	git pull

status:
	git status
	@echo "-----------------"
	clasp status | grep -v .git
