var Harvest = (function (){
  var API="https://api.harvestapp.com/api/v2";
  var harvest={};

  var _Invoices = (function (){
    var ENDPOINT="/invoices";
    var invoices = {};

    var _Payments = (function (){
      var payments ={};

      payments.list = function (){
        return _notimplemented();
      }

      payments.create = function (){
        return _notimplemented();
      }

      payments.del = function (){
        return _notimplemented();
      }

      payments.create = function (){
        return _notimplemented();
      }
      return payments;
    })();
    invoices.Payments = _Payments

    var _Messages = (function (){
      var messages = {};

      messages.list = function (){
        return _notimplemented();
      }

      messages.create = function (){
        return _notimplemented();
      }

      messages.del = function (){
        return _notimplemented();
      }

      messages.send = function (){
        return _notimplemented();
      }

      messages.close = function (){
        return _notimplemented();
      }

      messages.reopen = function (){
        return _notimplemented();
      }

      messages.open2draft = function (){
        return _notimplemented();
      }

      return messages;
    })();
    invoices.Messages = _Messages

    var _ItemCategories = (function() {
      var itemCategories ={};
      return itemCategories;
    })();
    invoices.ItemCategories = _ItemCategories

    invoices.list = function () {
      var URL=API+ENDPOINT
      var firstpage = _readpage(URL,1)
      var invoiceList=firstpage.invoices
      if (firstpage.total_pages > 1){
        for(var pp = 2; pp<= firstpage.total_pages; pp+=1){
          page = _readpage(URL,pp);
          var max=page.invoices.lenght
          for (var i=0; i<=max; i+=1){
            invoiceList.append(page.invoices[i]);
          }
        }
      }
      return invoiceList
    }

    invoices.getById = function (invoice_id){
      return _notimplemented();
    }

    invoices.createFreeForm = function (){
      return _notimplemented();
    }

    invoices.createFreeForm = function (){
      return _notimplemented();
    }

    invoices.create = function (){
      return _notimplemented();
    }

    invoices.update = function (){
      return _notimplemented();
    }

    invoices.createLineItem = function (){
      return _notimplemented();
    }

    invoices.updateLineItem = function (){
      return _notimplemented();
    }

    invoices.deleteLineItem = function (){
      return _notimplemented();
    }

    invoices.del = function (){
      return _notimplemented();
    }


    return invoices;

    function _notimplemented() {
      var name = "";
      var message = "function " + name + " not implemented.";
      console.info(message);
      return message;
    }

  })();
  harvest.Invoices = _Invoices

  var _TimeSheets = (function (){
    var timeSheets ={};

    var _TimeEntry = (function (){
      var timeEntry={};
//      timeEntry.list = function (user_id,client_id,project_id,is_billed,is_running,updated_since,from,to,page,per_page) {
      timeEntry.list = function () {
        return JSON.parse(
          UrlFetchApp.fetch("https://api.harvestapp.com/v2/time_entries",{
            "method": "GET",
            "headers": {
              "Authorization": "Bearer " + PropertiesService.getUserProperties().getProperty("accessToken"),
              "Harvest-Account-Id": PropertiesService.getUserProperties().getProperty("accountId"),
              "User-Agent": PropertiesService.getUserProperties().getProperty("appId")
            }
          }).getContentText()
        );
      };

      return timeEntry;
    })();
    timeSheets.TimeEntry = _TimeEntry

    return timeSheets;
  })();
  harvest.TimeSheets = _TimeSheets

  return harvest;
  // private methods

  function _readpage(url,page){
    if (page>1){
      url.append("?page="+page+"&per_page=100")
    }
    return JSON.parse(
      UrlFetchApp.fetch(url,{
        "method": "GET",
        "headers": _getCommonHeaders()
      }).getContentText()
    );
  }

  function _getCommonHeaders(){
    var P = PropertiesService.getUserProperties();
    var headers = {
      "Authorization": "Bearer " + P.getProperty("accessToken"),
      "Harvest-Account-Id": P.getProperty("accountId"),
      "User-Agent": P.getProperty("appId")
    };
    return headers
  };

  function pass() {};
})();
// vim: set ft=javascript sw=2 tw=0 fdm=syntax et :
